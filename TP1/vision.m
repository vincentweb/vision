clear

%im = imread('image3plans.png', 'png');
%im = imread('voie_ferree.png', 'png');
im = imread('image1.png', 'png');

hold on

%imshow(im);
%axis image;
axis ij;
axis off;
%axis equal
%flipud
imagesc(im);

%linfinis = [];



    [mouseX,mouseY] = ginput(4)
    f1=point_fuite(mouseX, mouseY)

    for i=1:4
        plot(mouseX(i), mouseY(i), 'o');
    end


    %plot(10, 10, 'o');

    f2=point_fuite(mouseX([1 3 2 4]), mouseY([1 3 2 4]))

    
    plot(f1(1), f1(2), 'o');



    plot([f1(1) mouseX(1) ], [f1(2) mouseY(1)], '-');
    plot([f1(1)  mouseX(2)], [f1(2)  mouseY(2)], '-');
    plot([f1(1) mouseX(3) ], [f1(2) mouseY(3)], '-');
    plot([f1(1)  mouseX(4)], [f1(2) mouseY(4)], '-');

 
    plot(f2(1), f2(2), 'o');
    plot([f2(1) mouseX(2) ], [f2(2) mouseY(2)], '-');
    plot([f2(1) mouseX(1) ], [f2(2) mouseY(1)], '-');
    plot([f2(1)  mouseX(4)], [f2(2)  mouseY(4)], '-');
    plot([f2(1) mouseX(3)], [f2(2)  mouseY(3)], '-');


    linfini = coordsToLine([f1(1) f2(1)], [f1(2) f2(2)]);

    plot([f1(1) f2(1)], [f1(2)  f2(2)], 'r-');
    linfini
    
 

hold off

pause(1);

linfini = linfini/linfini(3);
L = double([1 0 0; 0 1 0; linfini(1) linfini(2) linfini(3)]);

% IM3 = imtransform(im,maketform('projective',L),'FillValues', 0, 'Size', size(im));
% 
% axis ij;
% axis off;
% axis equal
% imagesc(IM3);
% 
% pause(2);
% 
% pdim = size(im,1)*size(im,2);
% [j,i]=ind2sub([size(im,2),size(im,1)],[1:pdim]);
% m  = reshape([i j ones(1,pdim)],pdim,3)';
% 
% im2coords = L*m;
% im2coords(1,:) = int16(im2coords(1,:)./im2coords(3,:));
% im2coords(2,:) = int16(im2coords(2,:)./im2coords(3,:));
% im2coords2 = (im2coords(1,:)+size(im,1)*(im2coords(2,:)-1));
% 
% im2coords3 = min(max(im2coords2,1),pdim);
% 
% %im2coords = reshape(int8(im2coords(1,:)), size(im,2), size(im,1))';
% 
% r=reshape(im(im2coords3), size(im,2), size(im,1))';
% g=reshape(im(im2coords3+pdim), size(im,2), size(im,1))';
% b=reshape(im(im2coords3+pdim*2), size(im,2), size(im,1))';
% 
% IM2 = cat(3,r, g, b);
% imagesc(IM2);




IM5 = zeros(size(im));


pause(2);

% LL = L^(-1);
% 
% for i=1:size(im,1)
%     for j=1:size(im,2)
%        coords  = LL * double([i; j; 1]);
%        coords = int16(round(coords/coords(3)));
%        if (coords(1)>0 && coords(1)>0 && coords(1)<=size(im,1) && coords(2)<=size(im,2))
%            IM5(i,j,:) = im(coords(1), coords(2),:);
%        end
%     end
% end
% axis ij;
% axis off;
% axis equal;
% imshow(IM5/255);


imout   = zeros(size(im));
hautin  = size(im,1); 
largin  = size(im,2); 
hautout = size(im,1); 
largout = size(im,2); 

L = L^(-1);


% On applique l'homographie à tous les pixels de notre image1
imout2  = CalculeImageHomographie(L,imout,hautout,largout,im,hautin,largin);
%colormap(gray);
imshow(imout2/255);