function p=point_fuite(mouseX, mouseY)

l1 = coordsToLine(mouseX(1:2), mouseY(1:2));
l2 = coordsToLine(mouseX(3:4), mouseY(3:4));
p = cross(l1, l2);
p = p/p(3);

return;