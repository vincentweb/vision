function l=coordsToLine(mouseX, mouseY)

% if (mouseX(1) == mouseX(2))
%     l = [1 0 mouseX(1)];
% else
%     a = (mouseY(2)-mouseY(1))/(mouseX(2)-mouseX(1));
%     b = mouseY(1) - a*mouseX(1);
%     l = [a 1 b];
% end

l = [mouseY(1)-mouseY(2), mouseX(2)-mouseX(1),  mouseX(1)*mouseY(2)-mouseX(2)*mouseY(1)];

% cross([X,1], [Y, 1])

return;