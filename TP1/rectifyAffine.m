function [I_affine, H] = rectifyAffine(hFig, I_distort, hFigRes)
    figure(hFig);
    
    % Have the user select two parallel lines, and find their intersection
    l11 = getLine(); addLine(l11, 'red');
    l12 = getLine(); addLine(l12, 'red');
    px1 = intersectLines(l11, l12);
    disp('Parallel lines lA ='), disp(l11'), disp(' and lB ='), disp(l12')
    disp('intersect at point p1 ='), disp(px1')

    % And a second set, and their intersection
    l21 = getLine(); addLine(l21, 'blue');
    l22 = getLine(); addLine(l22, 'blue');
    px2 = intersectLines(l21, l22);
    disp('Parallel lines lA ='), disp(l21'), disp(' and lB ='), disp(l22')
    disp('intersect at point p2 ='), disp(px2')

    % Now compute the line joining the two intersection points, this is the
    % image of the line at infinity.
    lx = lineFromPts(px1, px2);
    disp('The image of the line at infinity (through p1 and p2) =')
    disp(lx')

    % Setup the transform to map the line back to (0, 0, 1)
    H = [1 0 0; 0 1 0; lx'];
    disp('The rectifiying transform, H, is:'), disp(H)

    % Apply the transform to create the rectified image
    affine_tform = maketform('projective', H');
    I_affine  = imtransform(I_distort, affine_tform, ...
                            'FillValues', 0, 'Size', size(I_distort));

    % Show the results
    % The transformed lines are now disabled, they were not interesting...
    figure(hFigRes), clf, imshow(I_affine), title('Affine rectification');
    % addLine(transformLine(l11, H), 'red');
    % addLine(transformLine(l12, H), 'red');
    % addLine(transformLine(l21, H), 'blue');
    % addLine(transformLine(l22, H), 'blue');
end

function addLine(l, color)
    pos = get(gcf,'Position');
    widthX = pos(3);
    X = [0 widthX];
    Y = yForX(l, X);
    line(X,Y, 'Color', color);
end