% TRANSFORMIMAGE transforms the image from old coordinate system
% to a new one using the homography H
% new_img = TransformImage(img, H, o_x, o_y, description, pad)
% img is the image
% H is the homography that changes the coordinates
% o_x is the translation about x (if no change, this should be 0)
% o_y is the translation about y
% description goes into the title of the image to be drawn
% pad pads the image with pad amount of zeros

function new_img = transformImage(img, H, o_x, o_y, description, pad)

% Set the font size for all figure titles
font_size = 24;

% Pad the image
if ( pad > 0 )
    [rows cols] = size(img);
    img = [ repmat(0,rows,pad) img repmat(0,rows,pad) ];
    img = [ repmat(0,pad,cols+(2*pad)); img; repmat(0,pad,cols+(2*pad)) ];
end

% Set up the vectors for meshgrid
[rows cols] = size(img);
x = (1:cols)';
y = (1:rows)';

% MESHGRID provides [XI,YI] for use in 3-D plots
[XI,YI] = meshgrid(x,y);
XI_size = length(XI(:));

% Transform the coordinates
x_prime = H * [XI(:) YI(:) ones(XI_size,1)]';

% Divide out the z' or \lambda variable
X = x_prime(1,:) ./ x_prime(3,:);
Y = x_prime(2,:) ./ x_prime(3,:);

% If the coordinates have been transformed to be centered on the origin
% instead of the center of the original image, then change the original
% coordinate system to reflect this
if (o_x > 0 || o_y > 0)
    xi = (-o_x : cols - o_x - 1)';
    yi = (-o_y : rows - o_y - 1)';
    [XII, YII] = meshgrid(xi,yi);
else
    XII = XI;
    YII = YI;
end

% Get the new image from griddata
new_img_vector = griddata(X,Y,double(img(:)),XII(:),YII(:));

% Unstacks the new vector into a matrix
new_img = reshape(new_img_vector, rows, cols);

% Draw the new reshaped image
figure;
imagesc(XII(:),YII(:),new_img);colormap gray
axis on;

% Give the figure a title
name = sprintf( description );
title( name, 'FontSize', font_size );