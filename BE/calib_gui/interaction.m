% ******************************************************************************
% Ce script est le programme de saisie des sommets et des faces de l'escalier
% ******************************************************************************

% ******************************************************************************
% LES DIFFERENTS "OBJETS"
% ******************************************************************************
  % ------------------------------------------------------------------
  % FONCTIONS MATLAB
  % ------------------------------------------------------------------

  % ------------------------------------------------------------------
  % VARIABLES PERMANENTES EN ENTREE
  % ------------------------------------------------------------------

  % ------------------------------------------------------------------
  % VARIABLES PERMANENTES EN SORTIE
  % ------------------------------------------------------------------
    % points_2d, faces (dans interact.mat)

% ******************************************************************************
% CODE
% ******************************************************************************
  % ------------------------------------------------------------------
  % saisie des points
  % ------------------------------------------------------------------
    disp(' ');
    disp('Premi�re �tape: indiquer les points interessants sur les images');
    afficher_img_et_imd;

  % ------------------------------------------------------------------
  % saisie des faces
  % ------------------------------------------------------------------
  
  input(sprintf('AAAAAAAAAAAAAa'));
  
  disp(' ');
    disp('deuxi�me �tape: d�crire les faces');
    disp('quand vous avez fini, d�crivez une face vide');
    faces=[];
    no_face=1;
    while 1
      f=input(sprintf('points de la face %d:',no_face));
      [dummy,l_face]=size(f);

      % v�rification
      if l_face==0
        break;
      end
      ok=1;

      if l_face<3
        disp('un polygone comprend au moins 3 points: recommencez');
	ok=0;
      end

      for i=1:l_face
	if f(i)<1 | f(i)>n_points
	  disp(sprintf('le point %d ne me convient pas',f(i)));
	  ok=0;
        end
      end

      if ~ ok
	disp('vous avez une chance de recommencer');
	continue
      end

      f(l_face+1)=0;
      faces(no_face,1:l_face+1)=f;
      no_face=no_face+1;
    end

    disp('Merci d''avoir rempli avec application notre questionnaire');
    disp('je vais le sauver dans le fichier interact.mat');

    save interact points_2d faces
    disp('voil� qui est fait; au revoir, et toujours � votre service !');
    disp(' ');