
close all;

figure;
image1=imread('imaobj1_rect.jpg');
image(image1);
%imshow(image1/255);
hold on
title('image 1');

figure
image1=imread('imaobj2_rect.jpg');
image(image1);
%imshow(image1/255);
hold on
title('image 2');
% ------------------------------------------------------------------
% r�cup�ration des points 2D sur l'image 1
% ------------------------------------------------------------------
figure(1);
disp('Cliquez sur les points de l''image gauche');
disp(' puis appuyez sur <enter>');

% ...........................................................
% r�cup�ration des coordonn�es des points
% ...........................................................
fin=0;
i=1;
x_g=[];
y_g=[];
while fin==0
    [x,y]=ginput(1)

    if isempty(x)
        fin=1;
    else
        x_g=[x_g; x];
        y_g=[y_g; y];
% ...........................................................
% affichage des num�ros des points
% ...........................................................
        text(x,y,int2str(i),'Color','r')
        plot(x,y,'g+');
        i=i+1;
    end
end

n_points=length(x_g);
points_2d=zeros(n_points,4)
points_2d(:,1)=x_g
points_2d(:,2)=y_g


% ------------------------------------------------------------------
% r�cup�ration des points 2D sur l'image nord
% ------------------------------------------------------------------
figure(2);
disp(sprintf('Vous avez s�lectionn� %d points',n_points));
disp('passez � l''image de droite');

% ...........................................................
% r�cup�ration des coordonn�es des points
% ...........................................................
hold on
for (i=1:n_points)
    image(image1);
    title('image 2');
    %affiche les points deja rentr�s
    if i>1
        for(j=1:i-1)
            text(points_2d(j,3),points_2d(j,4),int2str(j),'Color','r')
                plot(points_2d(j,3),points_2d(j,4),'g+');
        end
    end

    [e2,w2]=calc_epipole(points_2d(i,1:2),c_1,c_2)
    [x_d,y_d]=ginput(1);
    points_2d(i,3)=x_d
    points_2d(i,4)=y_d

end

% ...........................................................
% affichage des num�ros des points final
% ...........................................................
image(image1);
title('image 2');
for(i=1:n_points)
    text(points_2d(i,3),points_2d(i,4),int2str(i),'Color','r')
end