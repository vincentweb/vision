function Baseline=CalcBaseline(c_1,c_2)

% En utilisant les formules en 2.3

c_tilde_d = c_1(1:3,1:3);
c_tilde_g = c_2(1:3,1:3);

c_tilde_4_g = c_1(1:3,4);
c_tilde_4_d = c_2(1:3,4);

% Position des centres optiques
Ld = - c_tilde_d^(-1) * c_tilde_4_d;
Lg = - c_tilde_g^(-1) * c_tilde_4_g;

Baseline = norm(Ld-Lg);

return;

