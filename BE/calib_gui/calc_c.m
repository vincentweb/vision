% ******************************************************************************
% Fonction d'une matrice de calibrage C, � partir des param�tres
% intris�ques et extrins�ques de la cam�ra
% ******************************************************************************

function c=calc_c(Tc,omc,fc,cc)
R=rodrigues(omc);
KK=[fc(1),0,cc(1),0;0, fc(2),cc(2),0;0,0,1,0]
M=[R];
%M(:,4)=-R*Tc;
M(:,4)=Tc;

M(4,:)=[0,0,0,1]
c=KK*M;
