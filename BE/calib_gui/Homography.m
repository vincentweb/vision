%Homographie pour la rectification de DataMatrix
close all %ferme les fenetres en cours
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%dimension de l'image de sortie 
larg=13 %largeur du datamatrix (= a la hauteur car carr�)
res=1 %nombre de points par carr� du datamatrix en x et en y
%cr�ation de l'image r�sultat
imout=zeros(larg*res,larg*res); 

nomimin='test1.bmp'; %nom du fichier image contenant le datamatrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%lecture de l'image d'entree
imin=imread(nomimin);
iminvisu=imin; %pour afficher dessus
[hautin,largin]=size(imin);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%d�finition des coordonn�es dans l'image d'origine (coordonn�es matlab commencent en 1,1)
figure
imshow(imin); %affichage de l'image
title('Clickez sur les 4 coins du datamatrix dans l''ordre');

Clicks=ginput(4); %recupere les 4 coordonn�es cliqu�es a la souris

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%commenter la ligne pr��dente pour ne pas avoir a cliquer sur les coins �
%chaque fois
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


close all %ferme les fenetres en cours
winty=5; %taille de la fenetre de recherche pour les coins
wintx=5;
[Xc,good,bad,type] = cornerfinder([Clicks(:,1)';Clicks(:,2)'],imin,winty,wintx); 
Xc=Xc';
%r�cup�re les coordonn�es individuellement
x11=Xc(1,1);
y11=Xc(1,2);
x12=Xc(2,1);
y12=Xc(2,2);
x13=Xc(3,1);
y13=Xc(3,2);
x14=Xc(4,1);
y14=Xc(4,2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%d�finition des coordonn�es  dans l'image finale(coordonn�es matlab
%commencent en 1,1)
x21=0.5;
y21=0.5;
x22=larg*res+0.5;
y22=0.5;
x23=larg*res+0.5;
y23=larg*res+0.5;
x24=0.5;
y24=larg*res+0.5;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
H=eye(3); %par defaut, copie des pixels sources vers l'image de destination
%Calculer la matrice H a partir des correspondances x11 a y14 et x21 a y25
% estim�e lin�aire par DLT

A = [x21 y21  1   0   0   0 -x21*x11 -y21*x11 -x11 ;
     0    0   0  x21 y21  1 -x21*y11 -y21*y11 -y11 ;
     x22 y22  1   0   0   0 -x22*x12 -y22*x12 -x12 ;
     0    0   0  x22 y22  1 -x21*y12 -y22*y12 -y12 ;
     x23 y23  1   0   0   0 -x23*x13 -y23*x13 -x13 ;
     0    0   0  x23 y23  1 -x23*y13 -y23*y13 -y13 ;
     x24 y24  1   0   0   0 -x24*x14 -y24*x14 -x14 ;
     0    0   0  x24 y24  1 -x24*y14 -y24*y14 -y14 ];
 
 [U, S, V] = svd(A);
 
 
 HH = V(:,size(V,2));
 

 
 H = reshape(HH, 3, 3)';
      


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%verification , les points doivent correspondre, donc la diffference entre un point
%et son homologue apres application de l'homographie doit �tre =0
%1� point
p2=H*[x21;y21;1];
[p2(1)/p2(3);p2(2)/p2(3)]-[x11;y11]
%2� point
p2=H*[x22;y22;1];
[p2(1)/p2(3);p2(2)/p2(3)]-[x12;y12]
%3� point
p2=H*[x23;y23;1];
[p2(1)/p2(3);p2(2)/p2(3)]-[x13;y13]
%4� point
p2=H*[x24;y24;1];
[p2(1)/p2(3);p2(2)/p2(3)]-[x14;y14]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%genere l'image du datamatrix rectifi�
imout=CalculeImageHomographie(H,imout,larg*res,larg*res,imin,hautin,largin);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%affichage
figure
subplot(2,2,2);
hold on
axis equal
axis ij
image(imout);
title('DataMatrix extrait');
colormap('Gray')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,2,1);
hold on
axis equal
axis ij
imshow(iminvisu);
hold on %surimpression par dessus l'image
plot(Clicks(:,1),Clicks(:,2),'r+'); %affichage des points click�s en rouge
plot([x11,x12,x13,x14],[y11,y12,y13,y14],'g+');%affichage des points apres raffinage en vert
title('Image d''origine');
colormap('Gray')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%modification du data matrix
%ecrire ce qu'on veut dans l'image avec imout(y,x)=val;
% A COMPLETER: modifier le datamatrix

%imout = zeros(size(imout));
sx = size(imout, 2);
sy = size(imout, 1);
smx = round(sx/2);
smy = round(sy/2);
size(imout)
for i = 4:-1:0
    [smx-i:smx+i, smy-i:smy+i]
    imout(smx-i:smx+i, smy-i:smy+i) = 255*(4-i)/4*ones(2*i+1, 2*i+1);
end



%on recopie l'image d'origine
imoutvisu=iminvisu;

%Calcul de l'homographie inverse
H2=eye(3); %par defaut, copie des pixels sources vers l'image de destination

A = [x11 y11  1   0   0   0 -x11*x21 -y11*x21 -x21 ;
     0    0   0  x11 y11  1 -x11*y21 -y11*y21 -y21 ;
     x12 y12  1   0   0   0 -x12*x22 -y12*x22 -x22 ;
     0    0   0  x12 y12  1 -x11*y22 -y12*y22 -y22 ;
     x13 y13  1   0   0   0 -x13*x23 -y13*x23 -x23 ;
     0    0   0  x13 y13  1 -x13*y23 -y13*y23 -y23 ;
     x14 y14  1   0   0   0 -x14*x24 -y14*x24 -x24 ;
     0    0   0  x14 y14  1 -x14*y24 -y14*y24 -y24 ];
 
 [U, S, V] = svd(A);
 
 
 HH = V(:,size(V,2));
 

 
 H2 = reshape(HH, 3, 3)';


%recalcule l'image de la cam�ra � partir du datamatrix
imoutvisu=CalculeImageHomographie(H2,iminvisu,hautin,largin,imout,larg*res,larg*res);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%affichage
%figure
subplot(2,2,4);
hold on
axis equal
axis ij
image(imout);
title('DataMatrix modifi�');
colormap('Gray')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,2,3);
hold on
axis equal
axis ij
imshow(imoutvisu);
title('Image augment�e');
hold on %surimpression par dessus l'image
plot(Clicks(:,1),Clicks(:,2),'r+'); %affichage des points click�s en rouge
plot([x11,x12,x13,x14],[y11,y12,y13,y14],'g+');%affichage des points apres raffinage en vert
colormap('Gray')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

