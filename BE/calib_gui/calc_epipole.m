function [e2,w2]=calc_epipole(m_1,c_1,c_2)
e2=[0,0,0];
w2=[0,0,0];

e1=[0,0,0];
w1=[0,0,0];

c_tilde_g = c_1(1:3,1:3);
c_tilde_d = c_2(1:3,1:3);

c_tilde_4_g = c_1(1:3,4);
c_tilde_4_d = c_2(1:3,4);


Lg = - c_tilde_g^(-1) * c_tilde_4_g;
Ld = - c_tilde_d^(-1) * c_tilde_4_d;

e2 = c_2*[Lg ; 1];
w2 = c_2*[c_tilde_g^(-1)*[m_1';1] ; 0];


%affichage d'une ligne
line([w2(1);e2(1)],[w2(2);e2(2)],'Color','r','LineWidth',1)


return;
