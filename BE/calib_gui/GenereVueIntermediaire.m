close all

for temps=0:0.1:1
    temps

    %calcule les matrices cam�ras 1 et 2
    c_1=calc_c(Tc_1,omc_1,fc,cc);
    c_2=calc_c(Tc_2,omc_2,fc,cc);

    C3_1=c_1(:,1:3); % recupere les 3 premieres colones, sous matrice 3*3 a gauche de c_1
    c4_1=c_1(:,4); % 4� colone de c_1

    C3_2=c_2(:,1:3); % recupere les 3 premieres colones, sous matrice 3*3 a gauche de c_2
    c4_2=c_2(:,4); % 4� colone de c_2

    %positions des centres optiques
    L1=inv(C3_1)*c4_1;
    L2=inv(C3_2)*c4_2;

    %position du centre optique de la cam�ra 3
    L3=L1+temps*(L2-L1);

    %pour les rotations, c'est l�gerement plus complexe
    %convertit les rotations en matrices
    R1=rodrigues(omc_1);
    R2=rodrigues(omc_2);

    %calcule la matrice correspondant � la rotation entre les 2 vues
    Rd=R1'*R2;
    %calcule le vecteur correspondant � la rotation complete
    omd=rodrigues(Rd);

    %calcule le vecteur correspondant � la rotation intermediaire relative
    %� R1
    omd2=temps*omd;
    %calcule la matrice correspondante relative � R1
    Rd2=rodrigues(omd2);

    %calcule la matrice rotation intermediaire par rapport au repere de
    %travail
    R3=R1*Rd2;
    %calcule le vecteur correspondant � la rotation intermediaire par 
    %rapport au repere de travail
    omc_3=rodrigues(R3);
    
    %calcule la translation exprim�e dans le rep�re cam�ra
    Tc_3=R3*L3;

    %calcule la matrice cam�ra 3
    c_3=calc_c(Tc_3,omc_3,fc,cc);
    
    %changement des param�tres intrins�ques
    %par exemple pour augmenter la focale
    %c_3=calc_c(Tc_3,omc_3,fc*1.5,cc);
    %par exemple pour d�placer le point principal
    %c_3=calc_c(Tc_3,omc_3,fc,cc+[300;-100]);


    %lit les 2 images
<<<<<<< HEAD
    image1=imread('imaobj1_rect.jpg');
    image2=imread('imaobj2_rect.jpg');
=======
    image1=imread('home1_obj_rect.png');
    image2=imread('home2_obj_rect.png');
>>>>>>> 1da139cc987ee54ca144d16ccee77ee8f1fa58c4

    %calcule des images en niveau de gris
    image1=0.33*image1(:,:,1)+0.33*image1(:,:,2)+0.33*image1(:,:,3);
    image2=0.33*image2(:,:,1)+0.33*image2(:,:,2)+0.33*image2(:,:,3);

    

    %pour avoir un fond avec les images 1 et 2 mix�es
<<<<<<< HEAD
    imagesynthese=(1-temps)*image1+temps*image2;
=======
    imagesynthese=0.7*((1-temps)*image1+temps*image2);
>>>>>>> 1da139cc987ee54ca144d16ccee77ee8f1fa58c4
    
    %pour avoir un fond noir
    %imagesynthese=0*image1;


    [nl,nc,nn]=size(image1);
    imout=zeros(nl,nc);


    %nombre de faces de l'objet
    nbfaces=size(faces,1);
%commencer avec la premi�re face de l'objet, puis commenter la ligne
%suivante pour traiter toutes les faces
   %nbfaces=6

   % FIXME
   
    for n=1:nbfaces-1


        if faces(n,1)==0
            break;
        end

        TailleTexture=256;
        imtexture=zeros(TailleTexture,TailleTexture);

      H=eye(3); %par defaut, copie des pixels sources vers l'image de destination

     % A COMPLETER: 
     
     display('calc')
     
     %Calculer les projections des points de cette face dans l'image 1
     %et l'homographie H avec l'image de texture de dimension
     %TailleTexture*TailleTexture
     face = faces(n,:);   
     v1 = points_3d(face(1),:);
     v2 = points_3d(face(2),:);
     v3 = points_3d(face(3),:);
     v4 = points_3d(face(4),:);
     
     p1 = c_3*[v1' ; 1];
     p2 = c_3*[v2' ; 1];
     p3 = c_3*[v3' ; 1];
     p4 = c_3*[v4' ; 1];
     
     p1 = p1/p1(3);
     p2 = p2/p2(3);
     p3 = p3/p3(3);
     p4 = p4/p4(3);
     
     x11 = p1(1,1);
     y11 = p1(2,1);
     x12 = p2(1,1);
     y12 = p2(2,1);
     x13 = p3(1,1);
     y13 = p3(2,1);
     x14 = p4(1,1);
     y14 = p4(2,1);
     
     display('une face')


     x11 = points_2d(face(1), 1);
     y11 = points_2d(face(1), 2);
     x12 = points_2d(face(2), 1);
     y12 = points_2d(face(2), 2);
     x13 = points_2d(face(3), 1);
     y13 = points_2d(face(3), 2);
     x14 = points_2d(face(4), 1);
     y14 = points_2d(face(4), 2);

    x21=0;
    y21=0;
    x22=TailleTexture;
    y22=0;
    x23=TailleTexture;
    y23=TailleTexture;
    x24=0;
    y24=TailleTexture;


     
     A = [x21 y21  1   0   0   0 -x21*x11 -y21*x11 -x11 ;
     0    0   0  x21 y21  1 -x21*y11 -y21*y11 -y11 ;
     x22 y22  1   0   0   0 -x22*x12 -y22*x12 -x12 ;
     0    0   0  x22 y22  1 -x21*y12 -y22*y12 -y12 ;
     x23 y23  1   0   0   0 -x23*x13 -y23*x13 -x13 ;
     0    0   0  x23 y23  1 -x23*y13 -y23*y13 -y13 ;
     x24 y24  1   0   0   0 -x24*x14 -y24*x14 -x14 ;
     0    0   0  x24 y24  1 -x24*y14 -y24*y14 -y14 ];



     [U, S, V] = svd(A);


     HH = V(:,size(V,2));



     H = reshape(HH, 3, 3)';

    
 
     
     
     
        %calcule l'image de texture en fonction de l'homographie fournie
        [imtexture]=CalculeImageHomographie(H,imtexture,TailleTexture,TailleTexture,image1,nl,nc);

        
        %Affichage de l'image de texture
        figure(1)
        clf
        hold on
        title('Image de texture calcul�e')
        imagesc(imtexture)
        colormap('Gray')
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
        
        H2=eye(3); %par defaut, copie des pixels sources vers l'image de destination

     % A COMPLETER: 
        
     %Calculer les projections des points de cette face dans l'image
     %intermediaire et l'homographie H2 avec l'image de texture de dimension
     %TailleTexture*TailleTexture 
     
     x21=0;
    y21=0;
    x22=TailleTexture;
    y22=0;
    x23=TailleTexture;
    y23=TailleTexture;
    x24=0;
    y24=TailleTexture;
    
    x11 = p1(1,1);
     y11 = p1(2,1);
     x12 = p2(1,1);
     y12 = p2(2,1);
     x13 = p3(1,1);
     y13 = p3(2,1);
     x14 = p4(1,1);
     y14 = p4(2,1);
     
     A = [x11 y11  1   0   0   0 -x11*x21 -y11*x21 -x21 ;
     0    0   0  x11 y11  1 -x11*y21 -y11*y21 -y21 ;
     x12 y12  1   0   0   0 -x12*x22 -y12*x22 -x22 ;
     0    0   0  x12 y12  1 -x11*y22 -y12*y22 -y22 ;
     x13 y13  1   0   0   0 -x13*x23 -y13*x23 -x23 ;
     0    0   0  x13 y13  1 -x13*y23 -y13*y23 -y23 ;
     x14 y14  1   0   0   0 -x14*x24 -y14*x24 -x24 ;
     0    0   0  x14 y14  1 -x14*y24 -y14*y24 -y24 ];
 
 [U, S, V] = svd(A);
 
 
 HH = V(:,size(V,2));
 

 
 H = reshape(HH, 3, 3)';
     
     
     
     
        [imagesynthese]=CalculeImageHomographie(H,imagesynthese,nl,nc,imtexture,TailleTexture,TailleTexture);


    end

    figure(2)
    hold on
    axis ij
    title('image g�n�r�e');
    imagesc(imagesynthese);
    colormap('Gray')
  
    %calcule les projections des points et affiche des croix sur l'image
    %synth�tis�e
    plot([0;800],[0;600],'g+');
    nbpts=size(points_3d,1);
    for n=1:nbpts
        %plot3(points_3d(n,1), points_3d(n,2),points_3d(n,3),'g+');
        M=[points_3d(n,1), points_3d(n,2),points_3d(n,3),1]';
        m=c_3*M;
        m=m/m(3);
        plot(m(1),m(2),'g+');
    end

    %sauvegarde l'image avec un nom correspondant au temps
    nomfichier=sprintf('video_t%f.jpg',temps)
    imwrite(imagesynthese,nomfichier,'jpg');

end