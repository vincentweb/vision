% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly excecuted under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 2160.119906589443417 ; 2156.926461714977449 ];

%-- Principal point:
cc = [ 404.961864487544688 ; 363.988954851460903 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ -0.244845448785127 ; 1.032979642992724 ; -0.001159598655600 ; 0.002152728846703 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 26.731465948298595 ; 24.443390743918865 ];

%-- Principal point uncertainty:
cc_error = [ 29.682175859596910 ; 29.505493733284897 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.079819532807364 ; 1.686847027082996 ; 0.003426122869314 ; 0.002490504011781 ; 0.000000000000000 ];

%-- Image size:
nx = 800;
ny = 600;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 10;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -3.102795e-01 ; -2.313129e+00 ; 1.008274e-01 ];
Tc_1  = [ 5.192714e+01 ; -1.299242e+02 ; 1.071947e+03 ];
omc_error_1 = [ 7.192859e-03 ; 1.352774e-02 ; 1.617532e-02 ];
Tc_error_1  = [ 1.479004e+01 ; 1.467398e+01 ; 1.253750e+01 ];

%-- Image #2:
omc_2 = [ 6.603061e-01 ; -2.258331e+00 ; -3.111261e-01 ];
Tc_2  = [ 2.945731e+01 ; -7.309678e+01 ; 1.096833e+03 ];
omc_error_2 = [ 8.214858e-03 ; 1.353774e-02 ; 1.625191e-02 ];
Tc_error_2  = [ 1.510205e+01 ; 1.500373e+01 ; 1.346877e+01 ];

%-- Image #3:
omc_3 = [ 2.714038e-01 ; -2.808746e+00 ; -2.495664e-01 ];
Tc_3  = [ 7.593708e+01 ; -6.191869e+01 ; 1.001092e+03 ];
omc_error_3 = [ 4.352368e-03 ; 1.436444e-02 ; 2.001833e-02 ];
Tc_error_3  = [ 1.378595e+01 ; 1.368756e+01 ; 1.158709e+01 ];

%-- Image #4:
omc_4 = [ 1.650543e+00 ; -2.528878e+00 ; -4.037891e-01 ];
Tc_4  = [ 1.243787e+02 ; 2.709663e+01 ; 9.429510e+02 ];
omc_error_4 = [ 9.350214e-03 ; 1.105367e-02 ; 2.098019e-02 ];
Tc_error_4  = [ 1.301615e+01 ; 1.292654e+01 ; 1.095760e+01 ];

%-- Image #5:
omc_5 = [ 8.909752e-01 ; -2.938919e+00 ; -5.112878e-01 ];
Tc_5  = [ 1.385966e+02 ; -5.374513e+01 ; 8.832673e+02 ];
omc_error_5 = [ 6.741352e-03 ; 1.238264e-02 ; 2.114593e-02 ];
Tc_error_5  = [ 1.218980e+01 ; 1.216509e+01 ; 1.082101e+01 ];

%-- Image #6:
omc_6 = [ -1.703390e-01 ; 2.685033e+00 ; 3.518831e-01 ];
Tc_6  = [ 1.365866e+02 ; -1.022230e+02 ; 8.941726e+02 ];
omc_error_6 = [ 4.602150e-03 ; 1.450951e-02 ; 1.786468e-02 ];
Tc_error_6  = [ 1.235390e+01 ; 1.229217e+01 ; 1.013958e+01 ];

%-- Image #7:
omc_7 = [ 4.646555e-01 ; 2.918122e+00 ; 6.253591e-01 ];
Tc_7  = [ 7.412118e+01 ; -1.036643e+02 ; 7.836760e+02 ];
omc_error_7 = [ 6.154368e-03 ; 1.413395e-02 ; 2.031894e-02 ];
Tc_error_7  = [ 1.081436e+01 ; 1.079823e+01 ; 9.781445e+00 ];

%-- Image #8:
omc_8 = [ 2.103586e+00 ; -1.990338e+00 ; -4.562433e-01 ];
Tc_8  = [ -4.718639e+01 ; 7.361392e+00 ; 1.154444e+03 ];
omc_error_8 = [ 1.265533e-02 ; 9.234330e-03 ; 2.184717e-02 ];
Tc_error_8  = [ 1.588123e+01 ; 1.580750e+01 ; 1.361987e+01 ];

%-- Image #9:
omc_9 = [ -2.543517e+00 ; 1.635935e+00 ; -4.188448e-02 ];
Tc_9  = [ -3.988688e+01 ; 2.876704e+01 ; 1.022995e+03 ];
omc_error_9 = [ 1.917236e-02 ; 1.337147e-02 ; 3.756398e-02 ];
Tc_error_9  = [ 1.405021e+01 ; 1.400426e+01 ; 1.218506e+01 ];

%-- Image #10:
omc_10 = [ -2.426792e+00 ; 1.375481e+00 ; 9.843268e-01 ];
Tc_10  = [ 7.423430e+01 ; 2.852189e+01 ; 8.236927e+02 ];
omc_error_10 = [ 1.003545e-02 ; 1.134199e-02 ; 1.886730e-02 ];
Tc_error_10  = [ 1.133250e+01 ; 1.128384e+01 ; 9.525741e+00 ];

