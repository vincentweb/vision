% ******************************************************************************
% Ce script donne un exemple de ce qu'il faut faire pour r�ussir � reconstruire
% un poly�dre � partir de la connaissance de tous les polyg�nes qui le composent
% ******************************************************************************

  figure;
  colors=['r' 'g' 'b' 'y'];

  for i=1:size(faces,1);
    x=[];
    y=[];
    z=[];
    n=1;
    %[faces(i,:)]
    while 1
      p=faces(i,n);
      if p==0
        break
      end
      p_c=points_3d(p,:);
      x(n)=p_c(1);y(n)=p_c(2);z(n)=p_c(3);
      n=n+1;
    end
    patch(x,y,z,colors(1+mod(i,length(colors))));
  end
  
  axis equal;
  rotate3d on;
