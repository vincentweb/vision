function imout=CalculeImageHomographie(H,imout,hautout,largout,imin,hautin,largin)
%fonction imout=CalculeImageHomographie(H,imout,hautout,largout,imin,hautin,largin)
%@Bertrand VANDEPORTAELE
%La fonction calcule l'image imout � partir de l'image imin
%H= homographie entre les 2 images tel que imout= H*imin
%imout= image de sortie, elle est aussi en entr�e, car on peut la modifier
%hautout,largout= dimensions de l'image de sortie
%imin= image d'entr�e
%hautin,largin= dimensions de l'image d'entr�e


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for y=1:hautout
    for x=1:largout
        p2=H*[x,y,1]'; %transformation homographique inverse
        ud=round(p2(1)/p2(3));
        vd=round(p2(2)/p2(3));
        ud;
        vd;
        if vd>0 & ud>0 & vd<=hautin & ud<=largin
            %ca tombe dans l'image
            val=imin(vd,ud);
            imout(y,x)=val; %on attribue la couleur du pixel concern�
        else
            %ca tombe hors de l'image datamatrix, on laisse la valeur du
            %fond
            %eventuellement, on aurait pu faire:
%            imout(y,x)=255; %on attribue la couleur blanche
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%