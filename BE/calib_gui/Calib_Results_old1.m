% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly excecuted under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 2219.135260813473900 ; 2188.992427858686400 ];

%-- Principal point:
cc = [ 479.571461265060460 ; 185.479493093051700 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ -0.315970071530754 ; 0.231632355833461 ; -0.001613445791597 ; 0.000747861580604 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 53.476193461416280 ; 53.911138014616185 ];

%-- Principal point uncertainty:
cc_error = [ 0.000000000000000 ; 0.000000000000000 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.072267860682205 ; 0.134025161522951 ; 0.009280680210979 ; 0.008340039568445 ; 0.000000000000000 ];

%-- Image size:
nx = 1600;
ny = 1200;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 7;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 0;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 1.693148e+000 ; 2.019479e+000 ; -8.645029e-001 ];
Tc_1  = [ 7.589698e+001 ; 3.213567e+001 ; 8.249725e+002 ];
omc_error_1 = [ 1.266487e-002 ; 1.056221e-002 ; 2.028228e-002 ];
Tc_error_1  = [ 9.117983e-001 ; 8.276731e-001 ; 1.642113e+001 ];

%-- Image #2:
omc_2 = [ 2.239931e+000 ; 1.075250e+000 ; -5.374392e-001 ];
Tc_2  = [ -1.129183e+001 ; 1.007555e+002 ; 7.768403e+002 ];
omc_error_2 = [ 1.528276e-002 ; 5.587229e-003 ; 1.854993e-002 ];
Tc_error_2  = [ 5.762505e-001 ; 1.349134e+000 ; 1.642742e+001 ];

%-- Image #3:
omc_3 = [ 3.077790e-001 ; 3.011415e+000 ; -2.681421e-001 ];
Tc_3  = [ 1.938672e+002 ; -1.647470e+001 ; 6.068674e+002 ];
omc_error_3 = [ 6.484645e-003 ; 2.314528e-002 ; 3.976908e-002 ];
Tc_error_3  = [ 7.968487e-001 ; 7.823244e-001 ; 1.411294e+001 ];

%-- Image #4:
omc_4 = [ 1.134037e-001 ; 2.786333e+000 ; -1.214708e+000 ];
Tc_4  = [ 2.008069e+002 ; 3.832136e+001 ; 6.063621e+002 ];
omc_error_4 = [ 4.757265e-003 ; 1.063368e-002 ; 1.607502e-002 ];
Tc_error_4  = [ 6.121696e-001 ; 7.014759e-001 ; 1.278213e+001 ];

%-- Image #5:
omc_5 = [ 3.115455e-001 ; 2.880579e+000 ; 9.899736e-001 ];
Tc_5  = [ 1.875729e+002 ; 6.741230e+000 ; 5.657295e+002 ];
omc_error_5 = [ 4.929832e-003 ; 9.233255e-003 ; 1.501458e-002 ];
Tc_error_5  = [ 4.943246e-001 ; 5.872595e-001 ; 1.417883e+001 ];

%-- Image #6:
omc_6 = [ 1.198650e+000 ; 1.920709e+000 ; 6.054911e-002 ];
Tc_6  = [ -1.936895e+001 ; 4.898287e+000 ; 6.471912e+002 ];
omc_error_6 = [ 7.050697e-003 ; 6.619711e-003 ; 1.084968e-002 ];
Tc_error_6  = [ 4.858449e-001 ; 6.150352e-001 ; 1.474168e+001 ];

%-- Image #7:
omc_7 = [ -2.214119e+000 ; -1.223040e+000 ; 1.519641e+000 ];
Tc_7  = [ 1.211666e+002 ; 8.492765e+001 ; 7.189022e+002 ];
omc_error_7 = [ 1.002795e-002 ; 1.059857e-002 ; 1.636788e-002 ];
Tc_error_7  = [ 1.063879e+000 ; 7.642988e-001 ; 1.478199e+001 ];

